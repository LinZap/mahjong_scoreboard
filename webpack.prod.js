const merge = require('webpack-merge'),
    webpack = require('webpack'),
    common = require('./webpack.common.js');

module.exports = merge(common, {
    plugins: [

        new webpack.optimize.UglifyJsPlugin({
            comments: false,
            compress: {
                warnings: false,
                drop_console: true
            },
        })
        ,
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ]
});