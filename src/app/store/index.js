import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";
import rootReducer from '../reducers'
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'

const loggerMiddleware = createLogger()
const sagaMiddleware = createSagaMiddleware()

export const History = createHistory()

const middleware = routerMiddleware(History)

export default function configureStore(initialState) {



  let middlewarelist = process.env.NODE_ENV === 'production' ? [
    sagaMiddleware,
    middleware] : [loggerMiddleware,
      sagaMiddleware,
      middleware]

  return {
    ...createStore(
      rootReducer,
      initialState,
      applyMiddleware(
        ...middlewarelist
      ),
    ),
    runSaga: sagaMiddleware.run,
  }
}