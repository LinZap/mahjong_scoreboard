const dealer_ron_point = require('./dealer_ron_point.json'),
    dealer_tsumo_point = require('./dealer_tsumo_point.json'),
    ron_point = require('./ron_point.json'),
    tsumo_dealer_point = require('./tsumo_dealer_point.json'),
    tsumo_point = require('./tsumo_point.json'),
    fumap = require('./fu_map.json');

/*
    han: han number
    fu: fu numbercls
    tsumo: true->tsumo, false->ron
    dealer: true-> is dealer, false-> is not dealer
 */

module.exports = function (han, fu, tsumo, dealer) {

    let p1, p2;

    try {

        han = Math.min(han, 13) - 1;
        fu = fumap[Math.min(fu, 110) + ""];

        if (tsumo) {
            if (dealer) {
                p1 = dealer_tsumo_point[han][fu];
                p2 = dealer_tsumo_point[han][fu];
            }
            else {
                p1 = tsumo_dealer_point[han][fu];
                p2 = tsumo_point[han][fu];
            }
        }
        else {
            if (dealer) {
                p1 = dealer_ron_point[han][fu];
                p2 = dealer_ron_point[han][fu];
            }
            else {
                p1 = ron_point[han][fu];
                p2 = ron_point[han][fu];
            }
        }

    }
    catch (e) {
        console.log('ERROR',han, fu, tsumo, dealer)
        return false
    }

    return (!!p1 && !!p2) ? [p1, p2] : false
}
