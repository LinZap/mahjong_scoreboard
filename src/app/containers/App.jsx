import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PlayerList from './PlayerList'

class App extends Component {
	render() {
		return (
			<div className="app">
				<PlayerList />
			</div>
		)
	}
}



export default App;