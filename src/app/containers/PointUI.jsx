import { connect } from 'react-redux'
import { 
    toggle_point_ui,
    set_fu,
    set_han,
    set_ron_target,
    player_tsumo,
    assign_point,
    next_inning,
} from '../actions';

import PointUI from '../components/PointUI';

const mapStateToProps = (state) => {
    const {pointui,game} = state
    return {
        pointui,
        game,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggle_point_ui: (id,tsumo,isopen) => dispatch(toggle_point_ui(id,false,tsumo,isopen)),
        set_fu: fu=>dispatch(set_fu(fu)),
        set_han: han=>dispatch(set_han(han)),
        set_ron_target: id=>dispatch(set_ron_target(id)),
        assign_point: data=>{
            dispatch(assign_point(data))
            dispatch(next_inning(data))
            dispatch(toggle_point_ui(4,false,false,false))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(PointUI)

