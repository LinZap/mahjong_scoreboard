import { connect } from "react-redux";

import { example_dataset } from "../actions";

import Header from '../components/Header';

const mapStateToProps =(state) =>{
    const {
        example
    } = state
    return {
        example
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        // eventfun : ()=> dispatch(action)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(Header)



