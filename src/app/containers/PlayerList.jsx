import { connect } from 'react-redux'
import { 
    player_richi, 
    player_draw, 
    toggle_point_ui,
    loss_inning,
    loss_inning_player,
    force_loss_inning,
    force_loss_inning_player,
    worng_ron,
    worng_ron_player,
} from '../actions';

import PlayerList from '../components/PlayerList';

const mapStateToProps = (state) => {
    const {
        player,
        game,
        pointui,
    } = state
    return {
        player,
        game,
        pointui,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        player_richi: id => dispatch(player_richi(id)),
        player_draw: id => dispatch(player_draw(id)),
        toggle_point_ui: (id, isdealer, tsumo, isopen) => dispatch(toggle_point_ui(id, isdealer, tsumo, isopen)),
        loss_inning: (player,game)=>{
            dispatch(loss_inning(player))
            dispatch(loss_inning_player(game))
        },
        force_loss_inning: (player)=>{
            dispatch(force_loss_inning(player))
            dispatch(force_loss_inning_player())
        },
        worng_ron: (id,isdealer,game)=>{
            dispatch(worng_ron(id,isdealer,game))
            dispatch(worng_ron_player({...game,id}))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps

)(PlayerList)



