import React, { Component } from 'react'
import { WIND_WORD } from '../actions'

export class Player extends Component {
	constructor(props) {
        super(props);
		this.handleWrongRon = this.handleWrongRon.bind(this);
	}

	handleWrongRon(event){
		let { player,data, game_wind, pid } = this.props,
			{ worng_ron,game } = data,
			{ name, wind, points, richi, burning, draw } = player,
			isdealer = (wind==0)
			if(confirm('錯和懲罰莊家自摸滿貫的點數，確定錯和嗎?')) worng_ron(pid,isdealer,game)
	}
	
	render() {
		const { player, game_wind, data, pid } = this.props,
			  { player_richi, player_draw, toggle_point_ui } = data,
			  { name, wind, points, richi, burning, draw } = player,
			  isdealer = (wind==0)
		return (
			<div className='block'>
				<p className='player-name'>{name}</p>
				<p className='wind'>{WIND_WORD[wind]}</p>
				<p className='score'>{points}</p>
				<p className='hint'>燒鳥：{burning ? 'YES' : 'NO'}</p>
				<p>
				{
					points < 1000 ? null:
					<p className='check'>
						<label><input type='checkbox' checked={richi} onClick={e=>player_richi(pid)} />立直</label>
					</p>
				}			
				<p className='check'>
					<label><input type='checkbox' checked={draw}  onClick={e=>player_draw(pid)}/>聽牌</label>
				</p>
				</p>
				<p className='f gap-top'>
					<button className='f1 btn' onClick={e=>toggle_point_ui(pid,isdealer,true,true)}>自摸</button>
					<button className='f1 btn' onClick={e=>toggle_point_ui(pid,isdealer,false,true)}>榮和</button>
					<button className='f1 btn' onClick={this.handleWrongRon}>錯和</button>
				</p>
			</div>
		)
	}
}




export default Player
