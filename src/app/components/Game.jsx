import React, { Component } from 'react'
import { WIND_WORD } from '../actions'

export class Game extends Component {
    render() {
        const { game } = this.props,
              { inning, round, offering, wind } = game
        return (
            <div className='f-col game'>
               <h2>{WIND_WORD[wind]} {inning} 局</h2>
               <p>{round} 本場</p>
               <p>供：{offering}</p>
            </div>
        )
    }
}

export default Game
