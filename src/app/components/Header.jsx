import React, { Component } from 'react'

export class Header extends Component {
  render() {
    const {example} =this.props
   
    return (
      <div className="header">
        {
          example.data.title
        }
      </div>
    )
  }
}

export default Header
