import React, { Component } from 'react'
import Player from './Player'
import Game from './Game'
import PointUI from '../containers/PointUI'
import { WIND_WORD } from '../actions'

export class PlayerList extends Component {

    constructor(props) {
        super(props);
        this.handleLossInning = this.handleLossInning.bind(this);
        this.handleForceLossInning = this.handleForceLossInning.bind(this);
        this.handleRestart = this.handleRestart.bind(this);   
    }

    handleLossInning(event) {
        let { player, game, loss_inning } = this.props
        if(confirm('確定要流局嗎?')) loss_inning(player,game)
    }

    handleForceLossInning(event){
        let { player, force_loss_inning} = this.props
        if(confirm('確定要強制流局嗎?')) force_loss_inning(player)
    }

    handleRestart(event){
        if(confirm('確定要重新開始嗎?')) location.reload()
    }

    render() {
        const { player, game, pointui } = this.props,
            { wind } = game,
            { isopen } = pointui

        return (

            <div className='board'>
                <div className='row'>
                    <div className='f-col' />
                    <button disabled={true} className='btn' title='敬請期待'>計分板</button>
                </div>
                <div className='row'>
                    <div className='f-col' />
                    <Player player={player[0]} game_wind={wind} pid={0} data={this.props} />
                    <div className='f-col' />
                </div>
                <div className='row'>
                    <Player player={player[1]} game_wind={wind} pid={1} data={this.props} />
                    <Game game={game} />
                    <Player player={player[3]} game_wind={wind} pid={3} data={this.props} />
                </div>
                <div className='row'>
                    <div className='f-col' />
                    <Player player={player[2]} game_wind={wind} pid={2} data={this.props} />
                    <div className='f-col' />
                </div>
                <div className='row gap-top'>
                    <div className='f-col' />
                    <button className='btn' onClick={this.handleLossInning}>流局</button>
                    <button className='btn' onClick={this.handleForceLossInning}>強制流局</button>
                    <button className='btn' onClick={this.handleRestart}>重新開始</button>
                </div>
                <div className='row gap-top author'>
                    
                    <a href='https://gitlab.com/LinZap/mahjong_scoreboard' target='_blank'>GitLab</a>． 
                    <a href='https://www.facebook.com/demonzap/' target='_blank'>Author</a>
                    <div className='f-col' />
                </div>
                {isopen ? <PointUI /> : null}

                
            </div>
        )
    }

}
export default PlayerList
