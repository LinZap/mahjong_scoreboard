import React, { Component } from 'react'
import { WIND_WORD } from '../actions'

export class PointUI extends Component {
    constructor(props) {
        super(props);
        this.handleSetRonTarget = this.handleSetRonTarget.bind(this);
        this.handleSetHan = this.handleSetHan.bind(this);
        this.handleSetFu = this.handleSetFu.bind(this);
        this.handleAssignPoints = this.handleAssignPoints.bind(this);
    }

    handleSetRonTarget(event) {
        let { set_ron_target } = this.props
        set_ron_target(event.target.value)
    }

    handleSetHan(event) {
        let { set_han } = this.props
        set_han(parseInt(event.target.value))
    }

    handleSetFu(event) {
        let { set_fu } = this.props
        set_fu(parseInt(event.target.value))
    }

    handleAssignPoints(event){
        let {pointui,assign_point,game} = this.props,
            {wind,round,offering} = game,
            data = {
                ...pointui,
                game_wind: wind,
                round,
                offering,
            }
        assign_point(data)
    }

    render() {
        let { pointui, toggle_point_ui, assign_point,game } = this.props,
            {round} = game,
            { isopen, tsumo, id, target, han, fu, result } = pointui,
            players = [0, 1, 2, 3],
            hans = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
            fus = [20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 110]

        return (
            <div className='cover' onClick={e => toggle_point_ui(0, false, false)}>
                <div className='lightbox' onClick={e => e.stopPropagation()}>

                    <div className='wind'>{tsumo ? 'ツモ' : 'ロン'}</div>

                    {tsumo ? null :
                        <div className='gap-top'><label>對象：<select value={target} onChange={this.handleSetRonTarget} >
                            <option value={4}>請選擇</option>
                            {players.filter(d => (d != id)).map(d => (
                                <option key={d} value={d}>{WIND_WORD[d]}</option>
                            ))}
                        </select></label></div>
                    }

                    <div className='gap-top'>
                        <label>
                            <select value={han} onChange={this.handleSetHan}>
                                {
                                    hans.map(d => (
                                        <option key={d} value={d}>{d}</option>
                                    ))
                                }
                            </select>飜
                        </label>

                        {
                            han > 4 ? null : <label>
                                <select value={fu} onChange={this.handleSetFu}>
                                    {
                                        fus.map(d => (
                                            <option key={d} value={d}>{d}</option>
                                        ))
                                    }
                                </select>符
                            </label>
                        }

                    </div>

                    <div className='gap-top'>
                        {
                            result? (
                                tsumo? 
                                (result[0]==result[1]?`每人 ${result[0]+round*100} 點`:`親家 ${result[0]+round*100} 點、子家 ${result[1]+round*100} 點`):
                                (`直擊 ${result[0]+round*300} 點`)
                            ):'不合法的飜符數'
                        }
                    </div>
                        
                    <div className='gap-top'>
                        <button className='btn' onClick={this.handleAssignPoints} disabled={!result||(tsumo?false:(target==4))}>確定執行</button>
                    </div>
                </div>
            </div>
        )
    }

}
export default PointUI
