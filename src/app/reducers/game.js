import {
    INIT_GAME,
    WIND,
    PLAYER_DRAW,
    NEXT_INNING,
    LOSS_INNING,
    FORCE_LOSS_INNING,
    WRONG_RON,
} from '../actions'

export default function (state = init_game_setting(), action) {

    let { offering, inning, round, wind } = state
    let { data } = action

    switch (action.type) {
        case INIT_GAME:
            return {
                ...state,
                ...init_game_setting()
            }

        case NEXT_INNING:
            return {
                ...state,
                ...go_next_inning(state,data),
            }

        case LOSS_INNING:
            return {
                ...state,
                ...lost_game(state,data),
            }
        
        case FORCE_LOSS_INNING:
            return {
                ...state,
                ...force_lost_game(state,data),
            }
        
        case WRONG_RON:
            let {isdealer} = action
            return {
                ...state,
                ...wrong_ron(state,isdealer),
            }
        default: return state
    }
}

const init_game_setting = () => ({
    wind: 0,
    inning: 1,
    round: 0,
    offering: 0
})

const go_next_inning = (game,{ isdealer, tsumo, id, target, han, fu, result, game_wind, round, offering })=>{
    let newdata = {
        ...game
    }
    newdata.offering = 0

    if(isdealer) newdata.round +=1
    else{
        newdata.round = 0;
        newdata.wind = newdata.inning==4? newdata.wind+1: newdata.wind;
        newdata.inning = newdata.inning==4? 1: (newdata.inning+1);       
    }
    return newdata
}


const lost_game = (game,player)=>{
    let newdata = {
        ...game
    }
    let draw = false

    for(let i=0;i<player.length;i++){
        let p = player[i]
        if(newdata.wind==p.wind) draw = p.draw
        if(p.richi) newdata.offering+=1
    }

    if(draw)newdata.round +=1
    else{
        newdata.round = 0;
        newdata.wind = newdata.inning==4? newdata.wind+1: newdata.wind;
        newdata.inning = newdata.inning==4? 1: (newdata.inning+1);
    }
    return newdata
}


const force_lost_game = (game,player)=>{
    let newdata = {
        ...game
    }
    let draw = false
    for(let i=0;i<player.length;i++){
        let p = player[i]
        if(p.richi) newdata.offering+=1
    }
    newdata.round +=1
    return newdata
}


const wrong_ron = (game,isdealer)=>{
    let newdata = {
        ...game
    }
    if(isdealer){
        
        newdata.wind = newdata.inning==4? newdata.wind+1: newdata.wind;
        newdata.inning = newdata.inning==4? 1: (newdata.inning+1);
    }
    return newdata
}