import calc_point from '../lib/clac_point'
import {
    TOGGLE_POINT_UI,
    SET_RON_TARGET,
    SET_FU,
    SET_HAN,
} from '../actions'



export default function (state = {
    isopen: false,
    tsumo: false,
    id: 0,
    target: 4,
    han: 1,
    fu: 20,
    isdealer: false,
    result: false,   
}, action) {

    let { isopen, tsumo, id, han, fu, target, isdealer } = action

    switch (action.type) {

        case TOGGLE_POINT_UI:
            isopen = isopen == undefined ? !isopen : isopen

            return {
                ...state,
                id: isopen? id: 0,    
                target: isopen? 4:state.target,
                han: isopen? 1:state.han,
                fu: isopen? 20:state.fu,
                tsumo: isopen? tsumo: false,              
                result: isopen? calc_point(1,20,tsumo,isdealer): state.result,
                isopen,
                isdealer,
            }

        case SET_RON_TARGET:
            return {
                ...state,
                target: id,
            }
        case SET_HAN:
            return {
                ...state,
                result: calc_point(han,state.fu,state.tsumo,state.isdealer),
                han,
            }
        case SET_FU:
            return {
                ...state,
                result: calc_point(state.han,fu,state.tsumo,state.isdealer),
                fu,
            }

        default: return state
    }
}