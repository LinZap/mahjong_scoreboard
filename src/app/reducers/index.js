import {combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import {reducer as formReducer} from 'redux-form'
import player from './player'
import game from './game'
import pointui from './pointui'

  const rootReducer = combineReducers({
    player,
    game,
    pointui,
    router: routerReducer,
    form:formReducer
  })
  
  
  export default rootReducer