import {
    INIT_GAME,
    WIND,
    PLAYER_RICHI,
    PLAYER_DRAW,
    ASSIGN_POINT,
    LOSS_INNING_PLAYER,
    FORCE_LOSS_INNING_PLAYER,
    WRONG_RON_PLAYER,
} from '../actions'

export default function (state = init_player_data(), action) {

    let { id, data } = action

    switch (action.type) {

        case INIT_GAME:
            return [
                ...init_player_data(),
            ]

        case PLAYER_RICHI:
            return [
                ...player_richi(state, id),
            ]

        case PLAYER_DRAW:
            return [
                ...player_draw(state, id),
            ]

        case ASSIGN_POINT:
            return [
                ...assign_point(state,data),
            ]
        
        case LOSS_INNING_PLAYER:
            return [
                ...lose_inning(state,data),
            ]
        
        case FORCE_LOSS_INNING_PLAYER:
            return [
                ...force_lose_inning(state),
            ]

        case WRONG_RON_PLAYER:
            return [
                ...wrong_ron_player(state,data),
            ]
        default: return state
    }
}



const model_player = (name, wind) => ({
    name,
    wind,
    points: 25000,
    richi: false,
    burning: true,
    draw: false,
})


const init_player_data = () => [
    model_player('A', WIND.E),
    model_player('B', WIND.S),
    model_player('C', WIND.W),
    model_player('D', WIND.N),
]

const player_richi = (players, id) => {
    let newdata = [...players]
    if(newdata[id].richi){
        newdata[id].richi = false
        newdata[id].points = newdata[id].points + 1000
        newdata[id].draw = false
    }
    else{
        newdata[id].richi = true
        newdata[id].points = newdata[id].points - 1000
        newdata[id].draw = true
    }  
    return newdata
}

const player_draw = (players, id) => {
    let newdata = [...players]
    newdata[id].draw = !newdata[id].draw
    return newdata
}

const assign_point = (players,{ isdealer, tsumo, id, target, han, fu, result, game_wind, round, offering }) => {

    let newdata = [...players],
        richi_point = offering*1000 

    for(let i=0;i<players.length;i++){
        let p = players[i]
        if(p.richi) richi_point+=1000 
        p.richi = false
        p.draw = false
    }
    
    newdata[id].burning = false
    
    if(isdealer){
        if(tsumo){
            newdata = newdata.map((p,i)=>{
                if(i==id) p.points+=(((result[0]+(100*round))*3)+richi_point)     
                else p.points-=(result[0]+(100*round))            
                return p
            })
        }
        else{
            newdata[id].points+=(result[0]+(300*round))+richi_point
            newdata[target].points-=(result[0]+(300*round))
        }
    }
    else{
        if(tsumo){
            newdata = newdata.map((p,i)=>{
                if(i==id) p.points+=((result[0]+(100*round))+(result[1]+(100*round))+richi_point)     
                else if(p.wind==game_wind) p.points-=(result[0]+(100*round))  
                else p.points-=(result[1]+(100*round))  
                return p
            })
        }
        else{
            newdata[id].points+=(result[0]+(300*round))+richi_point
            newdata[target].points-=(result[0]+(300*round))
        }

        for(let i=0;i<newdata.length;i++){
            let p = newdata[i];
            p.wind = p.wind==0? 3: (p.wind-1)
        }
    }

    return newdata;
}

const lose_inning = (players,game)=>{
    let newdata = [...players]
    let draw = false
    let draw_num = 0;

    for(let i=0;i<newdata.length;i++){
        let p = newdata[i]
        if(p.draw) draw_num+=1;
        if(p.wind==game.wind) draw = p.draw
    }
    // 罰符
    if(draw_num>0 && draw_num<4){
        let minuse_fu = 3000/(4-draw_num)
        let plus_fu = 3000/draw_num
        for(let i=0;i<newdata.length;i++){
            let p = newdata[i]
            if(p.draw) p.points+=plus_fu
            else p.points-=minuse_fu
        }
    }
    newdata = newdata.map(p=>({...p,richi:false,draw:false}))
    
    if(!draw)
        for(let i=0;i<newdata.length;i++){
            let p = newdata[i];
            p.wind = p.wind==0? 3: (p.wind-1)
        }   

    return newdata
}


const force_lose_inning = (players)=>{
    let newdata = [...players]
    newdata = newdata.map(p=>({...p,richi:false,draw:false}))
    return newdata
}


const wrong_ron_player = (players,game)=>{
    let newdata = [...players],
        isdealer = (newdata[game.id].wind==0)
    newdata[game.id].points -= isdealer? 12000: 8000
    newdata = newdata.map((p,i)=>{
        if(i!=game.id)p.points+=isdealer?4000:2000
        p.draw = false;
        p.richi = false;
        return p;
    })
    
    if(isdealer){
        for(let i=0;i<newdata.length;i++){
            let p = newdata[i];
            p.wind = p.wind==0? 3: (p.wind-1)
        }   
    }
    return newdata;
}