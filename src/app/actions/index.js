function action (type, payload = {}) {
    return {
      type,
      ...payload
    }
  }

// 遊戲規則與相關必要資料
export const WIND_WORD = ['東','南','西','北']
export const WIND = {
  E:0,
  S:1,
  W:2,
  N:3,
}

// 初始化相關
export const INIT_GAME =  'INIT_GAME'
export const init_game =  ()=>action(INIT_GAME)

// 遊戲進行相關

// 下一局
export const NEXT_INNING = 'NEXT_INNING'
export const next_inning = (data)=>action(NEXT_INNING,{data})

// 流局
export const LOSS_INNING = 'LOSS_INNING'
export const loss_inning = (data)=>action(LOSS_INNING,{data})

// 流局玩家
export const LOSS_INNING_PLAYER = 'LOSS_INNING_PLAYER'
export const loss_inning_player = (data)=>action(LOSS_INNING_PLAYER,{data})

// 強制流局
export const FORCE_LOSS_INNING = 'FORCE_LOSS_INNING'
export const force_loss_inning = (data)=>action(FORCE_LOSS_INNING,{data})

// 強制流局玩家
export const FORCE_LOSS_INNING_PLAYER = 'FORCE_LOSS_INNING_PLAYER'
export const force_loss_inning_player = ()=>action(FORCE_LOSS_INNING_PLAYER)

// 錯和
export const WRONG_RON = 'WRONG_RON'
export const worng_ron = (id,isdealer,data)=>action(WRONG_RON,{id,isdealer,data})

// 玩家錯和
export const WRONG_RON_PLAYER = 'WRONG_RON_PLAYER'
export const worng_ron_player = (data)=>action(WRONG_RON_PLAYER,{data})

// 玩家立直
export const PLAYER_RICHI = 'PLAYER_RICHI'
export const player_richi =  id=>action(PLAYER_RICHI,{id})

// 玩家聽牌
export const PLAYER_DRAW = 'PLAYER_DRAW'
export const player_draw = id=>action(PLAYER_DRAW,{id})

// 開啟算點面板
export const TOGGLE_POINT_UI = 'TOGGLE_POINT_UI'
export const toggle_point_ui = (id,isdealer,tsumo,isopen)=>action(TOGGLE_POINT_UI,{id,isdealer,tsumo,isopen})

// 玩家自摸
export const PLAYER_TSUMO = 'PLAYER_TSUMO'
export const player_tsumo = (id,han,fu,isdealer)=>action(PLAYER_TSUMO,{id,han,fu,isdealer})

// 設定玩家榮和對象
export const SET_RON_TARGET = 'SET_RON_TARGET'
export const set_ron_target = id=>action(SET_RON_TARGET,{id})

// 設定翻數
export const SET_HAN = 'SET_HAN'
export const set_han = han=>action(SET_HAN,{han})

// 設定符數
export const SET_FU = 'SET_FU'
export const set_fu = fu=>action(SET_FU,{fu})

// 賦予分數(生效計算結果)
export const ASSIGN_POINT = 'ASSIGN_POINT'
export const assign_point = data=>action(ASSIGN_POINT,{data})
