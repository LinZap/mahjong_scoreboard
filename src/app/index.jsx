import React from 'react';
import { render } from 'react-dom';
import styl from './styl/index.styl';
import { createStore } from 'redux';
import { Provider } from "react-redux";
import configureStore, { History } from "./store";
import rootSaga from "./sagas";
import { Route } from 'react-router-dom';

import { ConnectedRouter } from 'react-router-redux'

import App from './containers/App.jsx'
const store = configureStore()

store.runSaga(rootSaga)

render(
  <Provider store={store}>
    <ConnectedRouter history={History}>
      <Route path="/" component={App} />
    </ConnectedRouter>
  </Provider>

  , document.getElementById('root'));