const webpack = require('webpack'),
    path = require('path'),
    module_dir = `${__dirname}/node_modules`,
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    CleanWebpackPlugin = require('clean-webpack-plugin')
config = {
    entry: {
        app: [__dirname + '/src/app/index.jsx'],
        vendors: ['babel-polyfill', 'react', 'react-dom']
    },
    resolve: {
        extensions: ['.jsx', '.js', '.styl']
    },
    output: {
        path: __dirname + '/dist/',
        filename: '[name].[hash].js',
    },
    plugins: [
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendors",
            minChunks: module => module.context &&
                module.context.includes('node_modules'),
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'manifest'
        }),
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: `${__dirname}/src/www/index.html`,
            filename: 'index.html',
            inject: 'body',
        }),

    ],
    module: {
        rules: [{
            test: /\.jsx?$/,
            use: [{
                loader: 'react-hot-loader/webpack'
            }, {
                loader: 'babel-loader'
            }],
            exclude: /node_modules/
        }, {
            test: /\.styl?/,
            use: [{
                loader: 'style-loader'
            }, {
                loader: 'css-loader'
            }, {
                loader: 'postcss-loader'
            }, {
                loader: 'stylus-loader',options :{sourceMap: false }
            }]
        }, {
            test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
            loader: 'url-loader',
            options: {
                limit: 10000
            }
        },

        ]
    }
}

module.exports = config;