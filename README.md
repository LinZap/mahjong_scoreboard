# IQ 前端 react 開發 初始包

## 安裝說明

先 git 此包專案  再進入專案

### 安裝 node 相關套件 
```
 npm install
```
or 
```
npm i 
```

### 啟動開發模式
```
 npm run start
```

### 專案打包
會產生 一包打包後的專案 build
```
npm run build
```
## Node.js 環境

* node v8.9.4
* npm  v5.6.0


## 開發套件版本


* react @`^`16
* webpack @`^`3 
* react-router @`^`4
* react-saga @`^`0.16
* babel @`^`6.26
* react-redux @ `^`5
* redux-form @`^`7
